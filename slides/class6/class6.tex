\documentclass[9pt]{beamer}

\usepackage{amsmath}
%\usepackage{enumitem}

\title{Mathematical Optimization: Sufficient Optimality and Sensitivity}
\subtitle{CS/MATH 757/857}
\author{Marek Petrik}
\date{March 30}

\newcommand{\opt}{^\star}
\newcommand{\tr}{^\top}
\newcommand{\st}{\quad\operatorname{s.t.}\quad}
\newcommand{\eye}{I}

\setbeamertemplate{navigation symbols}{}
\usefonttheme{professionalfonts}

\begin{document}
	
\begin{frame}
	\maketitle
\end{frame}

\begin{frame}{Dual Cone}
	The dual cone to a cone $\mathcal{C}$ is defined as $\mathcal{C}\opt = \{ y ~:~ y\tr x \ge 0 \; \forall x \in \mathcal{C}\}$. And $\mathcal{C}^{\star\star} = \mathcal{C}$.\\
	\vspace{2.5in}
\end{frame}

\begin{frame}{Fundamental Necessary Condition}
	\begin{theorem}[12.3]
		If $x\opt$ is a local solution, then $\nabla f(x\opt)\tr d \ge 0$ for all $d\in T_\Omega(x\opt)$.
		%(or equivalently $\nabla f(x\opt) \in T\opt_\Omega(x\opt)$).
	\end{theorem}
	\vspace{2.5in}
\end{frame}

\begin{frame}{Why Fundamental Necessary Condition Insufficient}
	Examine at $(0,0)$:
	\[ \min_{x_1,x_2} x_2 \st x_2 \ge x_1^2 \]
	\vspace{2.5in}
\end{frame}

\begin{frame}{Constraint Qualification}
	\begin{lemma}[12.2]
		For a feasible $x\opt$:
		\begin{enumerate}
			\item $T_\Omega(x\opt) \subseteq \mathcal{F}(x\opt)$
			\item If LICQ then $T_\Omega(x\opt) = \mathcal{F}(x\opt)$
		\end{enumerate}
	\end{lemma}
	\vspace{2.4in}
\end{frame}

\begin{frame}{Farkas' Lemma}
	Take a convex cone $\mathcal{K} = \{ B y ~\mid~ y \ge 0 \}$
	\begin{theorem}[12.4]
		Given a $g\in\mathbb{R}^n$ then either $g\in \mathcal{K}$ or there exists $d\in\mathbb{R}^n$ such that $g\tr d < 0$ and $B\tr d \ge 0$, but not both.
	\end{theorem}

	\begin{theorem}[Rephrased]
		Given a $g\in\mathbb{R}^n$ then either $g\in \mathcal{K}$ or there exists $d\in\mathcal{K}\opt$ such that $g\tr d < 0$, but not both.
	\end{theorem}

	\begin{theorem}[Rephrased]
	Given a $g\in\mathbb{R}^n$ then:\\
	 $g\notin \mathcal{K}$ $\Longleftrightarrow$ $\exists d\in\mathcal{K}\opt$ such that $g\tr d < 0$.\\which is the same as:\\
	 $\nabla f(x\opt)\in \mathcal{K}(x\opt)$ $\Longleftrightarrow$ $\nabla f(x\opt)\tr d \ge 0$ for all $\forall d\in\mathcal{F}(x\opt)$  
	\end{theorem}
	
\end{frame}

\begin{frame}{Farkas' Lemma: What it says}
	Take a convex cone $\mathcal{K} = \{ B y ~\mid~ y \ge 0 \}$	
	\begin{theorem}[Rephrased]
		Given a $g\in\mathbb{R}^n$ then:
		$g\notin \mathcal{K}$ $\Longleftrightarrow$ $\exists d\in\mathcal{K}\opt$ such that $g\tr d < 0$.
	\end{theorem}
	\vspace{2.5in}
\end{frame}

\begin{frame}{Farkas' Lemma: The proof}
	Take a convex cone $\mathcal{K} = \{ B y ~\mid~ y \ge 0 \}$	
	\begin{theorem}[Rephrased]
		Given a $g\in\mathbb{R}^n$ then:
		$g\notin \mathcal{K}$ $\Longleftrightarrow$ $\exists d\in\mathcal{K}\opt$ such that $g\tr d < 0$.
	\end{theorem}
	\vspace{2.5in}
\end{frame}

\begin{frame}{Applying Farkas' Lemma}
		Take a convex cone 
		\[\mathcal{K}(x\opt) = \left\{ \sum_{i \in \mathcal{A}(x\opt)} \lambda_i \nabla c_i(x\opt) ~\mid~ \lambda_i \ge 0 \right\} = \mathcal{F}\opt(x\opt)\]
	\begin{theorem}[12.4]
		Given $\nabla f(x\opt)\in\mathbb{R}^n$ then either $\nabla f(x\opt)\in \mathcal{K}$ or there exists an improving direction $d\in\mathbb{R}^n$ such that $\nabla f(x\opt)\tr d < 0$ and $\nabla c_i(x\opt)\tr d \ge 0$, but not both.
	\end{theorem}
	
	\begin{theorem}[Rephrased]
		Given $\nabla f(x\opt)\in\mathbb{R}^n$ then either $\nabla f(x\opt)\in \mathcal{K}$ or there exists an improving $d\in\mathcal{K}\opt(x\opt) = \mathcal{F}(x\opt)$ such that $\nabla f(x\opt)\tr d < 0$, but not both.
	\end{theorem}
	
	\begin{theorem}[Rephrased]
		Given $\nabla f(x\opt)\in\mathbb{R}^n$ then:\\
		$\nabla f(x\opt)\notin \mathcal{K}(x\opt)$ $\Longleftrightarrow$ $\exists d\in\mathcal{F}(x\opt)$ such that $\nabla f(x\opt)\tr d < 0$, \\which is the same as:\\
		$\nabla f(x\opt)\in \mathcal{K}(x\opt)$ $\Longleftrightarrow$ $\nabla f(x\opt)\tr d \ge 0$ for all $\forall d\in\mathcal{F}(x\opt)$  
	\end{theorem}
	
\end{frame}

\begin{frame}{The Proof of Necessary First Order Condition}
	\begin{enumerate}
		\item Take a locally optimal $x\opt$
		\item Then by the fundamental condition: $\nabla f(x\opt)\tr d \ge 0$ for all $d\in T_\Omega(x\opt)$
		\item By LICQ $T_\Omega(x\opt) = \mathcal{F}(x\opt)$ and thus $\nabla f(x\opt)\tr d \ge 0$ for all $d\in \mathcal{F}(x\opt)$
		\item By Farkas' lemma: $\nabla f(x\opt)\in \mathcal{K}(x\opt)$ 
	\end{enumerate}
	\vspace{1.8in}
\end{frame}

\begin{frame}{Recall: Lagrangian}
	\[ \mathcal{L}(x,\lambda) = f(x) - \sum_{i\in\mathcal{I}} \lambda_i c_i(x) \]
	\vfill
	\[ \nabla_x \mathcal{L}(x,\lambda)  = \nabla_x f(x) - \sum_{i\in\mathcal{I}} \lambda_i \nabla_x c_i(x) \]
	\[ \nabla_{\lambda_i} \mathcal{L}(x,\lambda) = c_i(x) \]
\end{frame}

\begin{frame}{Critical Cone}
	Recall Linearized Feasible Directions at $x\opt$:
	\[ \mathcal{F}(x\opt) = \left\{ d ~:~
	d\tr \nabla c_i(x\opt) \ge 0 \quad \forall i \in \mathcal{A}(x\opt) \cap \mathcal{I} \right\}\]	
	\emph{Critical Cone} at $x\opt,\lambda\opt$:
	\[ \mathcal{C}(x\opt,\lambda\opt) = \{ w\in\mathcal{F}(x\opt) ~:~ \nabla c_i(x\opt)\tr w = 0 \text{ if } \mathcal{A}(x\opt) \cap \mathcal{I}  \text{ and } \lambda_i\opt > 0 \}\]
	\vspace{2in}
	
\end{frame}

\begin{frame}{Second-Order Necessary Condition}
	If $x\opt$ is a local solution, LICQ is satisfied, and KKT holds for $\lambda\opt$ then:
	\[ w\tr \nabla^2_{x x} \mathcal{L}(x\opt, \lambda\opt) w \ge 0, \text{ for all } w\in\mathcal{C}(x\opt,\lambda\opt)~.\]
	\emph{Question:} Is this also necessary:
	\[ w\tr \nabla^2_{x x} \mathcal{L}(x\opt, \lambda\opt) w \ge 0, \text{ for all } w\in\mathcal{F}(x\opt)~.\]
	\vspace{2.2in}
\end{frame}

\begin{frame}{Second-Order Sufficient Condition}
Suppose that $x\opt$ is feasible and KKT holds for $\lambda\opt$ and
\[ w\tr \nabla^2_{x x} \mathcal{L}(x\opt, \lambda\opt) w > 0, \text{ for all } w\in\mathcal{C}(x\opt,\lambda\opt) \setminus \{0\}~.\]
then $x\opt$ is a strict local solution.\\
\emph{Question:} Is this also sufficient:
\[ w\tr \nabla^2_{x x} \mathcal{L}(x\opt, \lambda\opt) w > 0, \text{ for all } w\in\mathcal{F}(x\opt) \setminus \{0\}~.\]
\vspace{2.2in}
\end{frame}

\begin{frame}{Sensitivity: Diet}
	What is the optimal diet (soylent anyone)? 
	\begin{itemize}
		\item Protein $x_1$, costs $\$3$
		\item Carbs $x_2$, costs $\$2$
		\item Nutritional needs:
		\begin{itemize}
			\item Calories: $x_1 + 2 x_2 \ge 3$
			\item Protein minimum: $x_1 \ge 1$
		\end{itemize}
	\end{itemize}
	Optimization problem:
	\[ \min_x 3 x_1 + 2 x_2 \st x_2 \ge 0, x_1 + 2 x_2 \ge 3, x_1 \ge 1 \]
	\emph{Question}: How sensitive solution is to changing caloric requirements:
 	\[x_1 + 2 x_2 \ge 3 - \epsilon\]
\end{frame}


\begin{frame}{Sensitivity}
	Let $x\opt$ be optimal and $x\opt(\epsilon)$ be optimal such that $c_i(x\opt(\epsilon)) = -\epsilon$. Constraints $c_j$ unchanged. Then:
	\begin{align*}
		-\epsilon &= c_i(x\opt(\epsilon)) - c_i(x\opt) \approx (x\opt(\epsilon) - x\opt)\tr \nabla c_i(x\opt) \\		
		0 &= c_j(x\opt(\epsilon)) - c_j(x\opt) \approx (x\opt(\epsilon) - x\opt)\tr \nabla c_j(x\opt), \quad j\neq i 
	\end{align*}
	The objective value:
	\begin{align*}
	f(x\opt(\epsilon)) - f(x\opt) &\approx (x\opt(\epsilon) - x\opt)\tr \nabla f(x\opt) \\
	&= \sum_{j\in\mathcal{A}(x\opt)} \lambda\opt_j(x\opt(\epsilon) - x\opt) \nabla c_j(x\opt) \\
	&= -\epsilon \lambda_i\opt
	\end{align*}
\end{frame}

\begin{frame}{Sensitivity}
		Optimization problem:
	\[ \min_x 3 x_1 + 2 x_2 \st x_2 \ge 0, x_1 + 2 x_2 \ge 3, x_1 \ge 1 \]
	\emph{Question}: How sensitive solution is to changing caloric requirements:
	\[x_1 + 2 x_2 \ge 3 - \epsilon\]
	\vspace{2in}
\end{frame}

\begin{frame}{Active and Binding Constraints}
	
	\emph{Strongly active, binding} if $i \in\mathcal{A}(x\opt)$ and $\lambda_i\opt > 0$ \\
	\vfill
	
	\emph{Weakly active} if $i \in\mathcal{A}(x\opt)$ and $\lambda_i\opt = 0$

\end{frame}

\end{document}
\documentclass[9pt]{beamer}

\usepackage{amsmath}
%\usepackage{enumitem}

\title{Mathematical Optimization: Necessary Optimality Proofs 3}
\subtitle{CS/MATH 757/857}
\author{Marek Petrik}
\date{March 27}

\newcommand{\opt}{^\star}
\newcommand{\tr}{^\top}
\newcommand{\st}{\quad\operatorname{s.t.}\quad}
\newcommand{\eye}{I}

\setbeamertemplate{navigation symbols}{}
\usefonttheme{professionalfonts}

\begin{document}
	
\begin{frame}
	\maketitle
\end{frame}

\begin{frame}{Convex Cone}
	A set $\mathcal{C}$ is a \emph{cone} if $\alpha x \in \mathcal{C}$ for $x\in\mathcal{C}$ and $\alpha \ge 0$. A \emph{convex cone} is a cone that is convex. \\
	\vspace{2.5in}
\end{frame}

\begin{frame}{Example Cones}
	\begin{enumerate}
		\item Affine: $\mathcal{C} = \{x\in \mathbb{R}^n ~\mid~A x \ge 0\}$
		\item Second Order: $\mathcal{C} = \{ (x,t) \in \mathbb{R}^{n+1} ~\mid~ \|x\|_2 \le t \}$
		\item Semidefinite cone: $\mathcal{C} = \{ A \in \mathbb{R}^{n\times n} ~\mid~ A \succeq 0\}$
		\item Copositive cone: $\mathcal{C} = \{  A \in \mathbb{R}^{n\times n} ~\mid~ x\tr A x \ge 0, ~\forall x\ge 0 \}$
	\end{enumerate}
\end{frame}

\begin{frame}{Paraphrase: KKT Conditions}
	Continually differentiable $f,c$ (remove equalities):
	\[ \min_x f(x) \st c_j(x) \ge 0,\, j \in \mathcal{I} \]
	If $x\opt$ is locally optimal and constraint qualification (LICQ) holds then:
	\begin{align*}
	\nabla f(x\opt) &= \sum_{i \in \mathcal{A}(x\opt)}  \lambda\opt_i \nabla c_i(x\opt) \\
	\lambda_i\opt &\ge 0 & i&\in\mathcal{I} \\
	c_i(x\opt) &\ge 0 & i&\in\mathcal{I}
	\end{align*}
	\textbf{Rephrase}: If $x\opt$ is locally optimal then $x\opt$ is feasible and $\nabla f(x\opt)$ is in the cone $\mathcal{K}(x\opt)$ spanned by $\nabla c_i(x\opt)$ for active $i\in\mathcal{A}(x\opt)$\\
	\vspace{1in}
\end{frame}

\begin{frame}{Dual Cone}
	The dual cone to a cone $\mathcal{C}$ is defined as $\mathcal{C}\opt = \{ y ~:~ y\tr x \ge 0 \; \forall x \in \mathcal{C}\}$. And $\mathcal{C}^{\star\star} = \mathcal{C}$.\\
	\vspace{2.5in}
\end{frame}

\begin{frame}{Tangent Cone}
	Let $\Omega$ be feasible set. The \emph{tangent cone} $T_\Omega(x\opt)$ for $x\opt\in\Omega$ is the set of tangents $d$ that satisfy:
	\[ \lim_{k\to\infty} (z_k - x\opt)/t_k = d~, \]
	for $z_k \to x\opt$, $z_k\in\Omega$, $t_k \to 0$, and $t_k \ge 0$.\\
	\vspace{2in}
\end{frame}

\begin{frame}{Linearized Feasible Directions}
	A convex cone:
	\[ \mathcal{F}(x\opt) = \left\{ d ~:~ \begin{matrix}
		d\tr \nabla c_i(x\opt) \ge 0 \quad &\forall i \in \mathcal{A}(x\opt) \cap \mathcal{I}
	\end{matrix} \right\}\]	
	\vspace{2in}
\end{frame}

\begin{frame}{Linearized Feasible Directions}
	$\mathcal{F}(x\opt)$ is \emph{dual cone} to convex cone spanned by $\nabla c_i(x\opt)$:
	\[ \operatorname{cone}\Bigl( \{\nabla c_i(x\opt) ,\, i \in \mathcal{I}\} \cup \{\nabla c_i(x\opt) ,\, i \in \mathcal{E}\}  \cup \{-\nabla c_i(x\opt) ,\, i \in \mathcal{E}\}\Bigr) \]
	\vspace{2in}
\end{frame}

\begin{frame}{Fundamental Necessary Condition}
	\begin{theorem}[12.3]
		If $x\opt$ is a local solution, then $\nabla f(x\opt)\tr d \ge 0$ for all $d\in T_\Omega(x\opt)$.
		%(or equivalently $\nabla f(x\opt) \in T\opt_\Omega(x\opt)$).
	\end{theorem}
	\vspace{2.5in}
\end{frame}

\begin{frame}{Why Fundamental Necessary Condition Insufficient}
	Examine at $(0,0)$:
	\[ \min_{x_1,x_2} x_2 \st x_2 \ge x_1^2 \]
	\vspace{2.5in}
\end{frame}

\begin{frame}{Constraint Qualification}
	\begin{lemma}[12.2]
		For a feasible $x\opt$:
		\begin{enumerate}
			\item $T_\Omega(x\opt) \subseteq \mathcal{F}(x\opt)$
			\item If LICQ then $T_\Omega(x\opt) = \mathcal{F}(x\opt)$
		\end{enumerate}
	\end{lemma}
	\vspace{2.4in}
\end{frame}

\begin{frame}{Farkas' Lemma}
	Take a convex cone $\mathcal{K} = \{ B y ~\mid~ y \ge 0 \}$
	\begin{theorem}[12.4]
		Given a $g\in\mathbb{R}^n$ then either $g\in \mathcal{K}$ or there exists $d\in\mathbb{R}^n$ such that $g\tr d < 0$ and $B\tr d \ge 0$, but not both.
	\end{theorem}

	\begin{theorem}[Rephrased]
		Given a $g\in\mathbb{R}^n$ then either $g\in \mathcal{K}$ or there exists $d\in\mathcal{K}\opt$ such that $g\tr d < 0$, but not both.
	\end{theorem}

	\begin{theorem}[Rephrased]
	Given a $g\in\mathbb{R}^n$ then:\\
	 $g\notin \mathcal{K}$ $\Longleftrightarrow$ $\exists d\in\mathcal{K}\opt$ such that $g\tr d < 0$.
	\end{theorem}
	
\end{frame}

\begin{frame}{Farkas' Lemma}
	Take a convex cone $\mathcal{K} = \{ B y ~\mid~ y \ge 0 \}$	
	\begin{theorem}[Rephrased]
		Given a $g\in\mathbb{R}^n$ then:
		$g\notin \mathcal{K}$ $\Longleftrightarrow$ $\exists d\in\mathcal{K}\opt$ such that $g\tr d < 0$.
	\end{theorem}
	\vspace{2.5in}
\end{frame}

\begin{frame}{Applying Farkas' Lemma}
		Take a convex cone 
		\[\mathcal{K}(x\opt) = \left\{ \sum_{i \in \mathcal{A}(x\opt)} \lambda_i \nabla c_i(x\opt) ~\mid~ \lambda_i \ge 0 \right\} = \mathcal{F}\opt(x\opt)\]
	\begin{theorem}[12.4]
		Given $\nabla f(x\opt)\in\mathbb{R}^n$ then either $\nabla f(x\opt)\in \mathcal{K}$ or there exists an improving direction $d\in\mathbb{R}^n$ such that $\nabla f(x\opt)\tr d < 0$ and $\nabla c_i(x\opt)\tr d \ge 0$, but not both.
	\end{theorem}
	
	\begin{theorem}[Rephrased]
		Given $\nabla f(x\opt)\in\mathbb{R}^n$ then either $\nabla f(x\opt)\in \mathcal{K}$ or there exists an improving $d\in\mathcal{K}\opt(x\opt) = \mathcal{F}(x\opt)$ such that $\nabla f(x\opt)\tr d < 0$, but not both.
	\end{theorem}
	
	\begin{theorem}[Rephrased]
		Given $\nabla f(x\opt)\in\mathbb{R}^n$ then:\\
		$\nabla f(x\opt)\notin \mathcal{K}(x\opt)$ $\Longleftrightarrow$ $\exists d\in\mathcal{F}(x\opt)$ such that $\nabla f(x\opt)\tr d < 0$, \\which is the same as:\\
		$\nabla f(x\opt)\in \mathcal{K}(x\opt)$ $\Longleftrightarrow$ $\nabla f(x\opt)\tr d \ge 0$ for all $\forall d\in\mathcal{F}(x\opt)$  
	\end{theorem}
	
\end{frame}

\begin{frame}{The Proof of Necessary First Order Condition}
	\begin{enumerate}
		\item Take a locally optimal $x\opt$
		\item Then by the fundamental condition: $\nabla f(x\opt)\tr d \ge 0$ for all $d\in T_\Omega(x\opt)$
		\item By LICQ $T_\Omega(x\opt) = \mathcal{F}(x\opt)$ and thus $\nabla f(x\opt)\tr d \ge 0$ for all $d\in \mathcal{F}(x\opt)$
		\item By Farkas' lemma: $\nabla f(x\opt)\in \mathcal{K}(x\opt)$ 
	\end{enumerate}
	\vspace{1.8in}
\end{frame}

\end{document}
# CS/MATH 757/857: Mathematical Optimization

## When

- *Mon*, *Wed*, *Fri*: 9:40 - 11:00 pm online


## Syllabus and Slides


| Date   | Day | Slides                                   | Reading | Annotated Slides
| ------ | --- | ---------------------------------------- | ------- | ----------------
| Mar 09 | Mon | Introduction to constrained optimization |   12.1  |
| Mar 11 | Wed | Necessary optimality conditions          |   12.1  |
| Mar 16 | Mon | *Spring Break*                           |         |
| Mar 18 | Wed | *Spring Break*                           |         |
| Mar 23 | Mon | KKT proof 1: Unconstrained optimum       |   12.3  | [Slides](slides/class3/class3-ann-comm.pdf)
| Mar 25 | Wed | KKT proof 2: Tangent cones               |   12.2  | [Slides](slides/class4/class4-ann-comm.pdf)
| Mar 27 | Fri | KKT Proof 3: Farkas' lemma               |   12.4  | [Slides](slides/class5/class5-ann-comm.pdf)
| Mar 30 | Mon | Sufficiency and Sensitivity              |   12.5-8| [Slides](slides/class6/class6-ann-comm.pdf)
| Apr 01 | Wed | Duality 1                                |   12.8  | [Slides](slides/class7/class7-ann-comm.pdf)
| Apr 03 | Fri | Duality 2                                |   12.9  | [Slides](slides/class8/class8-ann-comm.pdf)
| Apr 06 | Mon | Duality 3                                |   12.9  | [Slides](slides/class9/class9-ann-comm.pdf)
| Apr 08 | Wed | Linear programming: Duality and geometry |   13.1-2| [Slides](slides/class10/class10-ann-comm.pdf)
| Apr 11 | Fri | Discussion of HW solutions               |         | [Slides](slides/class11/class11-ann-comm.pdf)
| Apr 13 | Mon | Linear programming: Simplex              |   13.3-4| [Slides](slides/class12/class12-ann-comm.pdf)
| Apr 15 | Wed | Linear programming: Simplex others       |   13.5-8| [Slides](slides/class13/class13-ann-comm.pdf)
| Apr 17 | Fri | Simplex Q and A                          |         | [Slides](slides/class14/class14-ann-comm.pdf)
| Apr 20 | Mon | Interior point methods                   |   14.1  | [Slides](slides/class15/class15-ann-comm.pdf)
| Apr 22 | Wed | How to solve it: A tour                  |         | [slides](slides/class16/class16-ann-comm.pdf)
| Apr 24 | Fri | Canceled                                 |         | 
| Apr 27 | Mon | How to solve it: A tour                  |         | [Slides](slides/class17/class17-ann-comm.pdf)
| Apr 29 | Wed | Project presentations                    |         |
| Max 01 | Fri | Project presentations                    |         |
| May 04 | Mon | Review for the final exam                |         |


## Office Hours

On Zoom, Wed: 1:30-2:30

import numpy as np
from itertools import chain, combinations
# from https://docs.python.org/3/library/itertools.html
pset = lambda s: chain.from_iterable(combinations(range(s), r) for r in range(s+1))

# let x in R^5, solve:
# min 1/2 x^T A x
# s.t. 1^T x =  1    (d, lagrange multiplier xi)
#      r^T x >= mu   (c0, lambda0)
#          x >= 0    (c1,..,c5, lambda1, .., lambda5)

# setup problem matrices
As = np.random.rand(5,5); A = As.T @ As + 0.1*np.eye(5); r = [1,2,3,4,5]; mu = 3
# inequality constraint matrix (for all possible constraints)
B = np.row_stack( (r, np.eye(5))); b = np.array([mu,0,0,0,0,0])
for a in pset(6):
    # construct matrices with columns corresponding to variables
    #  x_1, x_2, ..., x_5, xi, lambda_1, .., lambda5
    # KKT condition nabla f - sum_i lambda_i nabla c_i
    G1 = np.column_stack((A, -np.ones(5), -B[a,:].T))
    g1 = np.zeros(G1.shape[0])
    # feasibility conditions  (add constraint d)
    G2 = np.column_stack((np.row_stack((B[a,:],np.ones(5))),np.zeros((len(a)+1,len(a)+1))))
    g2 = np.append(b[list(a)], [1,])
    # solve for x,xi,lambda
    try:
        xxilambda = np.linalg.solve(np.row_stack((G1,G2)),np.append(g1,g2))
        # check that lambda >= 0 and x >= 0
        if(np.min(xxilambda[:5]) >= -1e-4 and np.min(xxilambda[-5:]) >= -1e-4 and r @ xxilambda[:5] >= mu):
            print("Objective:", xxilambda[:5].T @ A @ xxilambda[:5], \
                "\nx", np.round(xxilambda,3)[:5], "\nxi:", np.round(xxilambda,3)[6], \
                "\nlambda:", np.round(xxilambda,3)[7:])
    except: pass
    #    print("Fail ", a)
